
//
//  ASTVGuideViewModel.swift
//  Astro
//
//  Created by NHT on 9/20/17.
//  Copyright © 2017 NHT. All rights reserved.
//

import RealmSwift

protocol ASTVGuideViewModelProtocol {
    
    var channelList: [Channel]? { get }
    // function to call when cities list did change
    var channelListDidChange: ((ASTVGuideViewModelProtocol) -> ())? { get set }
    
}

class ASTVGuideViewModel: ASChannelListViewModel {
    
    let dateFormatter = DateFormatter()
    
    // MARK: Time
    private func getEventStartDate(event: Event) -> Date? {
        guard let timeStart = event.displayDateTime else {
            return nil
        }
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.0"
        
        guard let startDate = dateFormatter.date(from: timeStart) else {
            return nil
        }
        return startDate
    }
    
    private func getTimeByMinutes(date: Date) -> Int {
        let components = Calendar.current.dateComponents([.hour, .minute], from: date)
        let hour = components.hour ?? 0
        let minutes = components.minute ?? 0
        return hour*60 + minutes
    }
    
    func getEventTimeStart(event: Event) -> String? {
        guard let startDate = getEventStartDate(event: event) else {
            return nil
        }
        dateFormatter.dateFormat = "HH:mm"
        return dateFormatter.string(from: startDate)
    }
    
    func getEventStartMinutes(event: Event) -> Int {
        guard let startDate = getEventStartDate(event: event) else {
            return 0
        }
        return getTimeByMinutes(date: startDate)
    }
    
    func getCurrentMinutes() -> Int {
        return getTimeByMinutes(date: Date())
    }
    
    // Get Event Duration in Minutes
    func getEventDuration(event: Event) -> Int {
        guard let timeArray = event.displayDuration?.components(separatedBy: ":"), timeArray.count > 1 else {
            return 0
        }
        let hour = Int(timeArray[0]) ?? 0
        let minutes = Int(timeArray[1]) ?? 0
        return hour * 60 + minutes
    }
    
}
