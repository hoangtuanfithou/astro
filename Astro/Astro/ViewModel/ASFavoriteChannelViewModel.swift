//
//  ASFavoriteChannelViewModel.swift
//  Astro
//
//  Created by NHT on 9/19/17.
//  Copyright © 2017 NHT. All rights reserved.
//

import RealmSwift
import FirebaseDatabase
import FirebaseAuth
import FacebookLogin

protocol ASFavoriteChannelViewModelProtocol: ASChannelListViewModelProtocol {
    
}

class ASFavoriteChannelViewModel: ASChannelListViewModel {
    
    func getChannelFavorite(_ sortType: SortType?) {
        let realm = try! Realm()
        let channels = realm.objects(Channel.self).sorted(byKeyPath: "channelTitle", ascending: false)
        channelList = Array(channels)
        sortChannelByType(sortType)
    }
    
    func getChannelFavoriteOnFireBase(_ sortType: SortType?) {
        guard let currentUserID = Auth.auth().currentUser?.uid else {
            return
        }
        ref.child(currentUserID).observe(.value, with: { snapshot in
            print(snapshot.value)
            var newItems: [Channel] = []
            for item in snapshot.children {
                let channel = Channel(snapshot: item as! DataSnapshot)
                newItems.append(channel)
            }
            self.channelList = newItems
            self.sortChannelByType(sortType)
        })
    }
    
    func signOut() {
        try? Auth.auth().signOut()
        loginManager.logOut()
        channelList = nil
    }
    
}
