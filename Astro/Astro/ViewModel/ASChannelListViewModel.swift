//
//  ASChannelListViewModel.swift
//  Astro
//
//  Created by NHT on 9/19/17.
//  Copyright © 2017 NHT. All rights reserved.
//

import RealmSwift
import SVProgressHUD
import FBSDKLoginKit
import FBSDKCoreKit
import FirebaseDatabase
import FirebaseAuth
import Firebase
import RxSwift

protocol ASChannelListViewModelProtocol {
    
    var channelList: [Channel]? { get }
    // function to call when cities list did change
    var channelListDidChange: ((ASChannelListViewModelProtocol) -> ())? { get set }

}

class ASChannelListViewModel: ASChannelListViewModelProtocol {
    
    /// Call to reload channels.
    var reload: AnyObserver<Void>

    var channelList: [Channel]? {
        didSet {
            channelListDidChange?(self)
        }
    }
    
    var channelListDidChange: ((ASChannelListViewModelProtocol) -> ())?
    let loginManager = FBSDKLoginManager()

    let channels: Observable<[Channel]>
    let alertMessage: Observable<String>
    let setCurrentLanguage: AnyObserver<String>
    let selectChannel: AnyObserver<Channel>
    let showChannel: Observable<Channel>

    func getChannelList() {
        SVProgressHUD.show()
        let channelListRequest = BaseRequest(ApiConfig.channelListUrl)
        _ = ApiBusiness.get(request: channelListRequest, responseClass: ChannelListResponse.self) { [weak self] (success, response) in
            SVProgressHUD.dismiss()
            if success, let channelListResponse = response {
                self?.channelList = channelListResponse.channels
                self?.sortChannelAtoZ()
            }
        }
    }
    
    // MARK: Sort
    func sortChannelByType(_ sortType: SortType?) {
        guard let sortType = sortType else {
            return
        }
        switch sortType {
        case .AtoZ:
            sortChannelAtoZ()
        case .ZtoA:
            sortChannelZtoA()
        case .ChannelNumberASC:
            sortChannelByChannelNumberASC()
        case .ChannelNumberDESC:
            sortChannelByChannelNumberDESC()
        }
    }
    
    private func sortChannelAtoZ() {
        channelList = channelList?.sorted(by: { $0.channelTitle ?? "" < $1.channelTitle ?? ""})
    }

    private func sortChannelZtoA() {
        channelList = channelList?.sorted(by: { $0.channelTitle ?? "" > $1.channelTitle ?? ""})
    }
    
    private func sortChannelByChannelNumberASC() {
        channelList = channelList?.sorted(by: { $0.channelStbNumber ?? "" < $1.channelStbNumber ?? "" })
    }
    
    private func sortChannelByChannelNumberDESC() {
        channelList = channelList?.sorted(by: { $0.channelStbNumber ?? "" > $1.channelStbNumber ?? "" })
    }

    // MARK: Favorite
    func saveChannelAsFavorite(_ channel: Channel) {
        let realm = try! Realm()
        if realm.object(ofType: Channel.self, forPrimaryKey: channel.channelId) != nil {
            return
        }
        try! realm.write() {
            realm.add(channel)
        }
    }
    
    // MARK: Login
    func loginFacebook(viewController: UIViewController, finish: @escaping (Bool, FBSDKLoginManagerLoginResult?) -> Void) {
        loginManager.logIn(withReadPermissions: ["public_profile"], from: viewController) { (loginResult, error) in
            guard error == nil else {
                finish(false, nil)
                return
            }
            let credential = FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)

            Auth.auth().signIn(with: credential) { (user, error) in
                guard error == nil else {
                    finish(false, nil)
                    return
                }
                // User is signed in
                finish(true, loginResult)
            }
        }
    }
    
    // MARK: Firebase
    var ref: DatabaseReference!

    init() {
        ref = Database.database().reference(withPath: "users")
        let aReload = PublishSubject<Void>()
        self.reload = aReload.asObserver()
        
        let _currentLanguage = BehaviorSubject<String>(value: "english")
        self.setCurrentLanguage = _currentLanguage.asObserver()

        let _alertMessage = PublishSubject<String>()
        self.alertMessage = _alertMessage.asObservable()
        
        self.channels = Observable.combineLatest(aReload, _currentLanguage) { _, language in language }
            .flatMapLatest { language in
                NetworkService().getChannels(byLanguage: language)
                    .catchError { error in
                        _alertMessage.onNext(error.localizedDescription)
                        return Observable.empty()
                }
            }
            .map { channels in channels.map(Channel.init) }
        
        let _selectChannel = PublishSubject<Channel>()
        self.selectChannel = _selectChannel.asObserver()
        self.showChannel = _selectChannel.asObservable()
            .map { $0 }
    }
    
    func saveChannelAsFavoriteOnFireBase(_ channel: Channel) {
        guard let currentUserID = Auth.auth().currentUser?.uid, let channelId = channel.channelId, let channelTitle = channel.channelTitle, let channelStbNumber = channel.channelStbNumber  else {
            return
        }
        
        ref.child(currentUserID).child(channelId).setValue(["channelTitle": channelTitle, "channelStbNumber": channelStbNumber])
    }
    
}
