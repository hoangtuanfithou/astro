//
//  ASCommon.swift
//  Astro
//
//  Created by NHT on 20/9/17.
//  Copyright © 2017 NHT. All rights reserved.
//

import Foundation
import UIKit

struct ApiConfig {
    
    static let channelListUrl = "http://ams-api.astro.com.my/ams/v3/getChannelList"
    static let getEventsUrl = "http://ams-api.astro.com.my/ams/v3/getEvents"
}

func LocalizedString(_ key: String) -> String {
    return NSLocalizedString(key, comment: "")
}

struct ASColor {
    
    static let blankColor = UIColor(white: 0.9, alpha: 1)
    static let timeColor = UIColor(red: 0.220, green: 0.471, blue: 0.871, alpha: 1)
    
}
