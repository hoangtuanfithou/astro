//
//  ASAlert.swift
//  Astro
//
//  Created by NHT on 9/25/17.
//  Copyright © 2017 NHT. All rights reserved.
//

import UIKit

class ASAlert {
    
    class func confirmYesNoAction(message: String, finish: @escaping (Bool) -> Void) {
        let alertController = UIAlertController(title: "Confirm", message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (result : UIAlertAction) -> Void in
            finish(false)
        }
        let okAction = UIAlertAction(title: "OK", style: .default) { (result : UIAlertAction) -> Void in
            finish(true)
        }
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        UIApplication.shared.delegate?.window??.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
}
