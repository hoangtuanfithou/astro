//
//  ASChannelListViewController.swift
//  Astro
//
//  Created by NHT on 9/19/17.
//  Copyright © 2017 NHT. All rights reserved.
//

import UIKit
import FacebookLogin
import FacebookCore
import MJRefresh
import DZNEmptyDataSet
import RxSwift
import RxCocoa

enum SortType: Int {
    case AtoZ, ZtoA, ChannelNumberASC, ChannelNumberDESC
}

class ASChannelListViewController: UIViewController {

    @IBOutlet weak private var tableView: UITableView!
    fileprivate var viewModel: ASChannelListViewModel!
    private let disposeBag = DisposeBag()
    private let refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupBindings()
        refreshControl.sendActions(for: .valueChanged)
    }
    
    /// Config load more and pull to refresh
    private func setupUI() {
        tableView.insertSubview(refreshControl, at: 0)
        
        // empty case
        tableView.emptyDataSetSource = self
    }
    
    private func setupBindings() {
        viewModel = ASChannelListViewModel()
        
        viewModel.channels
            .observeOn(MainScheduler.instance)
            .do(onNext: { [weak self] _ in self?.refreshControl.endRefreshing() })
            .bind(to: tableView.rx.items(cellIdentifier: "ChannelCell", cellType: UITableViewCell.self)) { [weak self] (_, repo, cell) in
                self?.setupChannelCell(cell, channel: repo)
            }
            .disposed(by: disposeBag)
        
        viewModel.showChannel
            .subscribe(onNext: { [weak self] in self?.openChannel(by: $0) })
            .disposed(by: disposeBag)
        
        refreshControl.rx.controlEvent(.valueChanged)
            .bind(to: viewModel.reload)
            .disposed(by: disposeBag)
        
        tableView.rx.modelSelected(Channel.self)
            .bind(to: viewModel.selectChannel)
            .disposed(by: disposeBag)
    }

    @IBAction func segmentValueChanged(_ sender: UISegmentedControl) {
        viewModel.sortChannelByType(SortType(rawValue: sender.selectedSegmentIndex))
    }
    
    private func setupChannelCell(_ cell: UITableViewCell, channel: Channel) {
        cell.textLabel?.text = channel.channelTitle
        cell.detailTextLabel?.text = channel.channelStbNumber
    }
    
    private func openChannel(by channel: Channel) {
        if AccessToken.current != nil {
            confirmAndAddFavorite(channel: channel)
        } else {
            viewModel.loginFacebook(viewController: self, finish: { [weak self] (isSuccess, loginResult) in
                if isSuccess {
                    self?.confirmAndAddFavorite(channel: channel)
                }
            })
        }
    }
    
    private func confirmAndAddFavorite(channel: Channel) {
        ASAlert.confirmYesNoAction(message: "Add this channel to favorite ?") { [weak self](isAccept) in
            self?.viewModel.saveChannelAsFavorite(channel)
            self?.viewModel.saveChannelAsFavoriteOnFireBase(channel)
        }
    }
    
}

// MARK: Handle for Empty data case
extension ASChannelListViewController: DZNEmptyDataSetSource {
    
    public func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let attributes = [NSFontAttributeName : UIFont.systemFont(ofSize: 12.0), NSForegroundColorAttributeName : UIColor.blue]
        return NSAttributedString(string: LocalizedString("NO_DATA"), attributes: attributes)
    }
    
    public func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return #imageLiteral(resourceName: "movie-banner-wide")
    }
    
}
