//
//  NetworkService.swift
//  Astro
//
//  Created by Nguyen Hoang Tuan on 10/23/17.
//  Copyright © 2017 NHT. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import ObjectMapper

enum ServiceError: Error {
    case cannotParse
}

/// A service that knows how to perform requests for GitHub data.
class NetworkService {
    
    private let session: URLSession
    
    init(session: URLSession = URLSession.shared) {
        self.session = session
    }
    
    func getChannels(byLanguage language: String) -> Observable<[Channel]> {
        let url = URL(string: ApiConfig.channelListUrl)!
        return session.rx
            .json(url: url)
            .flatMap { json throws -> Observable<[Channel]> in
                guard
                    let json = json as? [String: Any],
                    let itemsJSON = json["channels"] as? [[String: Any]]
                    else { return Observable.error(ServiceError.cannotParse) }
                
                let channels = Mapper<Channel>().mapArray(JSONArray: itemsJSON)
                return Observable.just(channels)
        }
    }
    
}
