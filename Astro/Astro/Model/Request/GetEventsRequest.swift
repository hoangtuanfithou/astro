//
//  GetEventsRequest.swift
//  Astro
//
//  Created by NHT on 9/20/17.
//  Copyright © 2017 NHT. All rights reserved.
//

import UIKit
import Foundation
import ObjectMapper

class GetEventsRequest: BaseRequest {
    
    var channelId: String?
    var periodStart: String?
    var periodEnd: String?
    
    convenience init(channelId: String?, url: String, date: Date) {
        self.init()
        self.url = url
        self.channelId = channelId
        
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "YYY-MM-dd"
        periodStart = dateFormatter.string(from: date) + " 0:00"
        periodEnd = dateFormatter.string(from: date) + " 23:59"
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        channelId <- map["channelId"]
        periodStart <- map["periodStart"]
        periodEnd <- map["periodEnd"]
    }
    
}
