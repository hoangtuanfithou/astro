//
//  Channel.swift
//
//  Create on 19/9/2017
//  Copyright © 2017 NHT. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift
import Alamofire
import FirebaseDatabase

class Channel: Object, Mappable, ChannelProtocol {

    dynamic var channelId: String?
    dynamic var channelStbNumber: String?
    dynamic var channelTitle: String?
    
    // MARK: Get Events
    private var eventDictionary = [Int: [Event]]()
    
    var eventListDidChange: ((ChannelProtocol) -> ())?
    private var request: Request?

    override static func primaryKey() -> String? {
        return "channelId"
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func getEventList(date: Date) -> [Event]? {
        let day = Calendar.current.component(.day, from: date)
        if eventDictionary[day] == nil && request?.task?.state != .running {
            getChannelEvents(date)
        }
        return eventDictionary[day]
    }
    
    func mapping(map: Map) {
        channelId <- (map["channelId"], TransformOf<String, Int>(fromJSON: { String($0 ?? 0) }, toJSON: { $0.map { Int($0) ?? 0 } }))
        channelStbNumber <- (map["channelStbNumber"], TransformOf<String, Int>(fromJSON: { String($0 ?? 0) }, toJSON: { $0.map { Int($0) ?? 0 } }))
        channelTitle <- map["channelTitle"]        
    }

    private func getChannelEvents(_ date: Date) {
        let eventListRequest = GetEventsRequest(channelId: channelId, url: ApiConfig.getEventsUrl, date: date)
        request = ApiBusiness.get(request: eventListRequest, responseClass: EventListResponse.self) { [weak self] (success, response) in
            if success, let eventListResponse = response, let me = self {
                let events = eventListResponse.events?.sorted(by: { $0.displayDateTime ?? "" < $1.displayDateTime ?? ""})
                me.eventDictionary[Calendar.current.component(.day, from: date)] = events
                me.eventListDidChange?(me)
            }
        }
    }
    
    // MARK: Firebase
    convenience init(snapshot: DataSnapshot) {
        self.init()
        if let postDict = snapshot.value as? [String : AnyObject] {
            channelId = snapshot.key
            channelTitle = postDict["channelTitle"] as? String
            channelStbNumber = postDict["channelStbNumber"] as? String
        }
    }

}

protocol ChannelProtocol {
    
    var eventListDidChange: ((ChannelProtocol) -> ())? { get set }
    func getEventList(date: Date) -> [Event]?
    
}
