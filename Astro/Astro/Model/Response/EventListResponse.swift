//
//  EventListResponse.swift
//
//  Create on 20/9/2017
//  Copyright © 2017 NHT. All rights reserved.
//

import Foundation
import ObjectMapper

class EventListResponse: BaseModel {

    var events: [Event]?
    var responseCode: String?
    var responseMessage: String?

    override func mapping(map: Map) {
        super.mapping(map: map)
        events <- map["getevent"]
        responseCode <- map["responseCode"]
        responseMessage <- map["responseMessage"]        
    }

}
