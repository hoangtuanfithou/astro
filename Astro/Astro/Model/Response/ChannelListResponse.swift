//
//  ChannelListResponse.swift
//
//  Create on 19/9/2017
//  Copyright © 2017 NHT. All rights reserved.
//

import Foundation
import ObjectMapper

class ChannelListResponse: BaseModel {

    var channels: [Channel]?
    var responseCode: String?
    var responseMessage: String?

    override func mapping(map: Map) {
        super.mapping(map: map)
        channels <- map["channels"]
        responseCode <- map["responseCode"]
        responseMessage <- map["responseMessage"]        
    }

}
