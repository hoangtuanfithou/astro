//
//  Getevent.swift
//
//  Create on 20/9/2017
//  Copyright © 2017 NHT. All rights reserved.
//

import Foundation
import ObjectMapper

class Event: BaseModel {

    var actors: String?
    var certification: String?
    var channelHD: Bool?
    var channelId: Int?
    var channelStbNumber: String?
    var channelTitle: String?
    var contentId: AnyObject?
    var contentImage: AnyObject?
    var directors: String?
    var displayDateTime: String?
    var displayDateTimeUtc: String?
    var displayDuration: String?
    var epgEventImage: AnyObject?
    var episodeId: String?
    var eventID: String?
    var genre: String?
    var groupKey: AnyObject?
    var highlight: AnyObject?
    var live: Bool?
    var longSynopsis: AnyObject?
    var ottBlackout: Bool?
    var premier: Bool?
    var producers: String?
    var programmeId: String?
    var programmeTitle: String?
    var shortSynopsis: String?
    var siTrafficKey: String?
    var subGenre: String?
    var vernacularData: [AnyObject]?

    override func mapping(map: Map) {
        super.mapping(map: map)
        actors <- map["actors"]
        certification <- map["certification"]
        channelHD <- map["channelHD"]
        channelId <- map["channelId"]
        channelStbNumber <- map["channelStbNumber"]
        channelTitle <- map["channelTitle"]
        contentId <- map["contentId"]
        contentImage <- map["contentImage"]
        directors <- map["directors"]
        displayDateTime <- map["displayDateTime"]
        displayDateTimeUtc <- map["displayDateTimeUtc"]
        displayDuration <- map["displayDuration"]
        epgEventImage <- map["epgEventImage"]
        episodeId <- map["episodeId"]
        eventID <- map["eventID"]
        genre <- map["genre"]
        groupKey <- map["groupKey"]
        highlight <- map["highlight"]
        live <- map["live"]
        longSynopsis <- map["longSynopsis"]
        ottBlackout <- map["ottBlackout"]
        premier <- map["premier"]
        producers <- map["producers"]
        programmeId <- map["programmeId"]
        programmeTitle <- map["programmeTitle"]
        shortSynopsis <- map["shortSynopsis"]
        siTrafficKey <- map["siTrafficKey"]
        subGenre <- map["subGenre"]
        vernacularData <- map["vernacularData"]        
    }

}
